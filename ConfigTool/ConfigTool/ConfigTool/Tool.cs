﻿using System;
using System.IO;

namespace ConfigTool
{
    public static class Tool
    {
        /// <summary>
        /// 创建文件
        /// </summary>
        /// <param name="_path">文件路径(包含文件名及其后缀)</param>
        /// <param name="_content">文件存储的内容</param>
        public static void CreateFile(string _path, string _content)
        {
            FileStream file = new FileStream(_path, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(file);
            writer.WriteLine(_content);
            writer.Close();
            file.Close();
            Console.WriteLine(string.Format("生成{0}", _path));
        }

        /// <summary>
        /// 删除指定文件夹下的子文件（不含子文件夹）
        /// </summary>
        /// <param name="_folderPath">文件夹路径</param>
        public static void ClearFiles(string _folderPath)
        {
            Console.WriteLine("开始删除文件，文件夹目录为" + _folderPath);
            if (!Directory.Exists(_folderPath))
            {
                Console.WriteLine("指定的文件路径不存在");
                return;
            }
            var files = Directory.GetFiles(_folderPath);
            foreach (var a in files)
            {
                File.Delete(a);
            }
            Console.WriteLine("删除完成");
        }

        /// <summary>
        /// 获取当前exe的上层几层路径
        /// </summary>
        /// <param name="_upperNum">向上几级</param>
        /// <returns>路径</returns>
        public static string GetPath(int _upperNum)
        {
            string exePath = Directory.GetCurrentDirectory();
            string[] temp = exePath.Split("\\".ToCharArray());
            string path = string.Empty;
            for (int i = 0; i < temp.Length - _upperNum; i++)
            {
                path += temp[i];
                path += "\\";
            }
            return path;
        }

        /// <summary>
        /// 创建指定目录，如目录存在则不创建
        /// </summary>
        /// <param name="_path">目录</param>
        public static void CreateDirectory(string _path)
        {
            if (!Directory.Exists(_path))
            {
                Directory.CreateDirectory(_path);
            }
        }
    }
}
