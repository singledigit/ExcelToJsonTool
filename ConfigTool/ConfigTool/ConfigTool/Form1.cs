﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace ConfigTool
{
    public partial class Form1 : Form
    {
        private string excelPath;
        private string jsonPath;
        private string codePath;

        public Form1()
        {
            InitializeComponent();
            InitPath();
        }

        private void InitPath()
        {
            excelPath = Tool. GetPath(4) + "ConfigExcel";
            jsonPath = Tool.GetPath(5) + "Assets\\Resources\\Config";
            codePath = Tool.GetPath(5) + "Assets\\Scripts\\Config";
            folderPath.Text = excelPath;
        }

        private void createJsonAndCode_Click(object sender, EventArgs e)
        {
            ExcelToJson etoJson = new ExcelToJson(excelPath, jsonPath);
            etoJson.CreateJsonFiles();
            fileNum.Text = etoJson.GetFileCount().ToString();

            AutoCreateCode createCode = new AutoCreateCode(codePath, etoJson.JsonDic);
            createCode.CreateCodeFiles();
        }
    }
}
