﻿namespace ConfigTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderPath = new System.Windows.Forms.Label();
            this.createJsonAndCode = new System.Windows.Forms.Button();
            this.fileNum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // folderPath
            // 
            this.folderPath.AllowDrop = true;
            this.folderPath.AutoSize = true;
            this.folderPath.Location = new System.Drawing.Point(39, 26);
            this.folderPath.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.folderPath.Name = "folderPath";
            this.folderPath.Size = new System.Drawing.Size(137, 12);
            this.folderPath.TabIndex = 1;
            this.folderPath.Text = "显示配置表文件夹的路径";
            // 
            // createJsonAndCode
            // 
            this.createJsonAndCode.Location = new System.Drawing.Point(41, 75);
            this.createJsonAndCode.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.createJsonAndCode.Name = "createJsonAndCode";
            this.createJsonAndCode.Size = new System.Drawing.Size(184, 52);
            this.createJsonAndCode.TabIndex = 2;
            this.createJsonAndCode.Text = "开始生成json和脚本文件";
            this.createJsonAndCode.UseVisualStyleBackColor = true;
            this.createJsonAndCode.Click += new System.EventHandler(this.createJsonAndCode_Click);
            // 
            // fileNum
            // 
            this.fileNum.AllowDrop = true;
            this.fileNum.AutoSize = true;
            this.fileNum.Location = new System.Drawing.Point(39, 47);
            this.fileNum.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.fileNum.Name = "fileNum";
            this.fileNum.Size = new System.Drawing.Size(53, 12);
            this.fileNum.TabIndex = 3;
            this.fileNum.Text = "文件数量";
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(362, 202);
            this.Controls.Add(this.fileNum);
            this.Controls.Add(this.createJsonAndCode);
            this.Controls.Add(this.folderPath);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConfigTool";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label folderPath;
        private System.Windows.Forms.Button createJsonAndCode;
        private System.Windows.Forms.Label fileNum;
    }
}

