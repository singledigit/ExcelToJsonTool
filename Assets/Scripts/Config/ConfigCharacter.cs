using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using SimpleJSON;
namespace ConfigTool
{
	public class ConfigCharacterData
	{
		/// <summary>
		/// 描述
		/// </summary>
		public readonly string description;
		/// <summary>
		/// 力量
		/// </summary>
		public readonly float power;
		/// <summary>
		/// 敏捷
		/// </summary>
		public readonly float agility;
		/// <summary>
		/// 智力
		/// </summary>
		public readonly float brains;
		/// <summary>
		/// 初始生命
		/// </summary>
		public readonly float initHp;
		/// <summary>
		/// 初始魔法
		/// </summary>
		public readonly float initMagic;
		/// <summary>
		/// 力量成长
		/// </summary>
		public readonly float powerIncrease;
		/// <summary>
		/// 敏捷成长
		/// </summary>
		public readonly float agilityIncrease;
		/// <summary>
		/// 智力成长
		/// </summary>
		public readonly float brainsIncrease;
		/// <summary>
		/// 速度
		/// </summary>
		public readonly float speed;
		/// <summary>
		/// 技能列表
		/// </summary>
		public readonly List<int> skillIdList = new List<int>();
		public ConfigCharacterData(JSONNode _jsonNode)
		{
			description = _jsonNode["description"];
			power = float.Parse(_jsonNode["power"]);
			agility = float.Parse(_jsonNode["agility"]);
			brains = float.Parse(_jsonNode["brains"]);
			initHp = float.Parse(_jsonNode["initHp"]);
			initMagic = float.Parse(_jsonNode["initMagic"]);
			powerIncrease = float.Parse(_jsonNode["powerIncrease"]);
			agilityIncrease = float.Parse(_jsonNode["agilityIncrease"]);
			brainsIncrease = float.Parse(_jsonNode["brainsIncrease"]);
			speed = float.Parse(_jsonNode["speed"]);
			string skillIdListValue = _jsonNode["skillIdList"];
			string[] skillIdListValues = skillIdListValue.Split(',');
			foreach(var a in skillIdListValues)
			{
				skillIdList.Add(int.Parse(a));
			}
		}
	}

	public class ConfigCharacter : ConfigBase
	{
		private Dictionary<int, ConfigCharacterData> dic = new Dictionary<int, ConfigCharacterData>();
		public ConfigCharacter(JSONClass _json)
		{
			for (int i = 3; i < _json.Count; i++)
			{
				int key = i - 1;
				ConfigCharacterData data = new ConfigCharacterData(_json[i]);
				dic.Add(key, data);
			}
		}
		public ConfigCharacterData GetData(int _id)
		{
			if (!dic.ContainsKey(_id))
			{
				Debug.LogError("没有指定Id");
				return null;
			}
			return dic[_id];
		}
	}
}

